-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 01 juil. 2018 à 00:28
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données :  `cesiphp`
--
CREATE DATABASE IF NOT EXISTS `cesiphp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cesiphp`;

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` varchar(36) NOT NULL,
  `client` varchar(50) NOT NULL,
  `category` set('Illustration','Graphic Design','Identity','Branding','Website Design','Photography') NOT NULL,
  `name` varchar(50) NOT NULL,
  `summary` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `image` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `projects`
--

INSERT INTO `projects` (`id`, `client`, `category`, `name`, `summary`, `description`, `date`, `image`) VALUES
('5d0320e7-7cb3-11e8-8de0-00fff3f0ddd9', 'Explore', 'Graphic Design', 'Project name', 'Lorem ipsum dolor sit amet consectetur.', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!', '2017-07-01', 'img/portfolio/02-full.jpg'),
('5d036897-7cb3-11e8-8de0-00fff3f0ddd9', 'Finish', 'Identity', 'Project name', 'Lorem ipsum dolor sit amet consectetur.', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!', '2017-07-01', 'img/portfolio/03-full.jpg'),
('859179f6-7cb3-11e8-8de0-00fff3f0ddd9', 'Lines', 'Branding', 'Project name', 'Lorem ipsum dolor sit amet consectetur.', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!', '2017-07-01', 'img/portfolio/04-full.jpg'),
('af79f496-7cb3-11e8-8de0-00fff3f0ddd9', 'Southwest', 'Website Design', 'Project name', 'Lorem ipsum dolor sit amet consectetur.', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!', '2017-07-01', 'img/portfolio/05-full.jpg'),
('d1a096bc-7cb3-11e8-8de0-00fff3f0ddd9', 'Window', 'Photography', 'Project name', 'Lorem ipsum dolor sit amet consectetur.', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!', '2018-07-01', 'img/portfolio/06-full.jpg'),
('d1fae5ac-7cb2-11e8-8de0-00fff3f0ddd9', 'Threads', 'Illustration', 'Project name', 'Lorem ipsum dolor sit amet consectetur.', 'Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!', '2017-01-01', 'img/portfolio/01-full.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` varchar(36) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`id`, `title`, `image`, `description`) VALUES
('835f6798-7cb1-11e8-8de0-00fff3f0ddd9', 'E-Commerce', 'fa-shopping-cart', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit. E-commercium gagnare pecunios.'),
('bad671a3-7cb1-11e8-8de0-00fff3f0ddd9', 'Responsive Design', 'fa-laptop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.'),
('bad68b83-7cb1-11e8-8de0-00fff3f0ddd9', 'Web Security', 'fa-lock', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(36) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL COMMENT 'result of password_hash php function',
  `name` varchar(200) NOT NULL,
  `job` varchar(100) NOT NULL,
  `picture` varchar(500) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `linkedin` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `job`, `picture`, `twitter`, `facebook`, `linkedin`) VALUES
('ca3562b1-7cb6-11e8-8de0-00fff3f0ddd9', 'dpertersen', '$2y$10$Y9EXapFYo/nzpg8x6.8uoud.ijF0Tq970YZO.GWWh1YRO75h4QOFe', 'Diana Pertersen', 'Lead Developer', 'img/team/3.jpg', '#', '#', '#');
COMMIT;
