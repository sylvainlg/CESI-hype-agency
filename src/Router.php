<?php

class Router {

	public function route($uri) {

		// controleur par défaut : SiteController
		$controller = 'site';
		$method = $uri;

		$explodedRoute = explode('/', $uri);
		if(count($explodedRoute) > 1) {
			$controller = array_shift($explodedRoute);
		}
		$method = array_shift($explodedRoute);

		$ctrlName = ucfirst($controller).'Controller';
		if(file_exists(ROOTDIR . '/src/controller/'.$ctrlName.'.php')) {
			include_once ROOTDIR . '/src/controller/'.$ctrlName.'.php';
			
			$ctrl = new $ctrlName();

			if(!method_exists($ctrl, $method)) {
				$this->notFound();
			} else {
				call_user_func([$ctrl, $method]);
			}

		} else {

			$this->notFound();

		}

	}

	private function notFound() {
		include_once ROOTDIR . '/src/controller/SiteController.php';
		$siteController = new SiteController();
		$siteController->page404();
	}

}

