# Mini site de support pour cours de PHP

Ce site est entièrement réalisé en HTML. La demande du client est la suivante :

> Bonjour, j’ai créé un site internet pour ma startup avec un freelance. Mon site ne convient plus, il faudrait le rendre modifiable facilement. Je ne suis pas très technique. Et j’aimerai que mes associés puissent aussi le modifier. Le formulaire de contact est cassé aussi.

> Pouvez vous m’aider ?
